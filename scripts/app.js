//// Намалювати на екрані поле 8*8 (можна використовувати таблицю чи набір блоків).
//// Згенерувати на полі випадковим чином 10 мін. Користувач не бачить, де вони знаходяться.
//// Клік лівою кнопкою по осередку поля "відкриває" її вміст користувачеві.
//// Якщо в цій клітинці знаходиться міна, гравець програв. У такому разі показати всі інші міни на полі. Інші дії стають недоступними, можна лише розпочати нову гру.
//// Якщо міни немає, показати цифру - скільки мін знаходиться поруч із цїєю клітинкою.
//// Якщо клітинка порожня (поряд з нею немає жодної міни) - необхідно відкрити всі сусідні клітинки з цифрами.
//// Клік правої кнопки миші встановлює або знімає із "закритої" клітинки прапорець міни.
//// Після першого ходу над полем має з'являтися кнопка "Почати гру заново", яка обнулюватиме попередній результат проходження та заново ініціалізуватиме поле.
//// Над полем має показуватись кількість розставлених прапорців, та загальна кількість мін (наприклад 7 / 10).

// Необов'язкове завдання підвищеної складності
//// При подвійному кліку на клітинку з цифрою - якщо навколо неї встановлено таку ж кількість прапорців, що зазначено на цифрі цієї комірки, відкривати всі сусідні комірки.
//// Додати користувачеві можливість самостійно вказувати розмір поля. Кількість мін на полі можна вважати за формулою Кількість мін = кількість осередків / 6.

//IDEAS:
//Timer

// const button = document.querySelector('button');
// const div = document.querySelector('div');
// const minutes = document.querySelector('[data-minutes]');
// const seconds = document.querySelector('[data-seconds]');
// let time = minutes.textContent * 60 + +seconds.textContent;
// console.log(time);
// button.addEventListener('click', (ev) => {
// 	const timer = setInterval(function () {
// 		console.log(--time);
// 		seconds.textContent = String(time % 60).padStart(
// 			2,
// 			'0'
// 		);
// 		if (time % 60 === 59 && minutes.textContent != 0) {
// 			minutes.textContent = String(
// 				minutes.textContent - 1
// 			).padStart(2, '0');
// 		}
// 		if (time <= 0) clearInterval(timer);
// 	}, 1000);
// 	button.textContent = 'Stop';
// });

createMenu();

function createMenu() {
	const form = document.querySelector('.menu-form');

	form.addEventListener('click', (e) => {
		e.preventDefault();

		if (e.target.dataset.name == 'start') {
			// console.clear();
			form.style.borderColor = null;

			const h =
				document.getElementById('field-height').value;
			const w =
				document.getElementById('field-width').value;
			const checkTable =
				document.querySelector('[data-dims]');

			if (
				h &&
				!isNaN(h) &&
				h > 2 &&
				w &&
				!isNaN(w) &&
				w > 2
			) {
				e.target.textContent = 'Restart game';

				if (!checkTable) {
					createTable(h, w);
				} else {
					deleteField(checkTable);
					createTable(h, w);
				}
			} else {
				form.style.borderColor = 'red';
			}
		}
	});
}

function createTable(x, y) {
	const tbl = document.createElement('table');
	const tblBody = document.createElement('tbody');

	const cellWidth = 50;
	const minesAmount = Math.floor((x * y) / 6);
	const playingField = createField(
		x,
		y,
		minesAmount,
		null,
		8
	);

	tbl.dataset.dims = `${x}x${y}`;

	createHints(playingField, x, y);
	// console.table(playingField);

	for (let i = 0; i < x; i++) {
		createRow(tblBody, y, cellWidth, i, playingField);
	}

	tbl.addEventListener('click', tableAction);
	tbl.addEventListener('contextmenu', setFlag);
	tbl.addEventListener('dblclick', openCellsNearFlag);

	tbl.append(tblBody);
	document.querySelector('.container').append(tbl);

	showCounters();
}

function tableAction(e) {
	if (e.target.dataset.hasOwnProperty('id')) {
		if (e.target.dataset.id === '0') {
			const coords = e.target.dataset.coords.split('-');
			const dims = document
				.querySelector('table')
				.dataset.dims.split('x');
			const x = dims[0];
			const y = dims[1];
			openCells(coords, x, y, 'openEmpty');
		}

		e.target.classList.remove('flagged');

		e.target.dataset.id === 'mine'
			? lose()
			: (e.target.className = 'cell');

		showCounters();
	}
}

function setFlag(e) {
	e.preventDefault();
	if (
		e.target.dataset.hasOwnProperty('id') &&
		e.target.classList.contains('cell-covered')
	) {
		e.target.classList.toggle('flagged');
		showCounters();
	}
}

function openCellsNearFlag(e) {
	const id = e.target.dataset.id;
	const coords = e.target.dataset.coords.split('-');
	const cells = document.querySelectorAll('[data-coords]');
	const dims = document
		.querySelector('[data-dims]')
		.dataset.dims.split('x');

	if (!isNaN(id) && parseInt(id) > 0) {
		let flags = openCells(
			coords,
			dims[0],
			dims[1],
			'countFlags'
		);

		if (parseInt(id) === flags) {
			openCells(
				coords,
				dims[0],
				dims[1],
				'openNearFlags'
			);
			showCounters();
		}
	}
}

function createRow(
	parent,
	cellsAmount,
	cellDim,
	indexX,
	field
) {
	const row = document.createElement('tr');

	for (let j = 0; j < cellsAmount; j++) {
		createCell(row, cellDim, indexX, j, field);
	}

	parent.append(row);
}

function createCell(
	parent,
	cellDim,
	indexX,
	indexY,
	field
) {
	const cell = document.createElement('td');

	cell.classList.add('cell-covered');
	cell.dataset.coords = `${indexX}-${indexY}`;

	if (field[indexX][indexY] === 'm') {
		cell.dataset.id = 'mine';
	} else {
		cell.dataset.id = field[indexX][indexY];
		if (field[indexX][indexY] !== 0)
			cell.textContent = field[indexX][indexY];
	}

	cell.style.height = `${cellDim}px`;
	cell.style.width = `${cellDim}px`;

	parent.append(cell);
}

function createField(maxHeight, maxWidth, m, f, condition) {
	if (!f) f = [];
	let field = [...f];

	let mines = m;

	for (let i = 0; i < maxHeight; i++) {
		if (!field[i]) field[i] = [];
		for (let j = 0; j < maxWidth; j++) {
			const random = Math.floor(Math.random() * 10);

			if (field[i][j] !== 'm') {
				if (random > condition && mines !== 0) {
					field[i][j] = 'm';
					mines--;
				} else if (field[i][j] === undefined)
					field[i][j] = 0;
			}
		}
	}
	if (mines !== 0) {
		field = createField(
			maxHeight,
			maxWidth,
			mines,
			field,
			condition - 1
		);
	}

	return field;
}

function createHints(f, x, y) {
	for (let i = 0; i < x; i++) {
		for (let j = 0; j < y; j++) {
			if (f[i][j] === 'm') {
				// top row
				if (i > 0 && j > 0 && f[i - 1][j - 1] !== 'm')
					f[i - 1][j - 1] += 1;
				if (i > 0 && f[i - 1][j] !== 'm')
					f[i - 1][j] += 1;
				if (
					i > 0 &&
					j < y - 1 &&
					f[i - 1][j + 1] !== 'm'
				)
					f[i - 1][j + 1] += 1;
				// mid row
				if (j > 0 && f[i][j - 1] !== 'm')
					f[i][j - 1] += 1;
				if (j < y - 1 && f[i][j + 1] !== 'm')
					f[i][j + 1] += 1;
				// bot row
				if (
					i < x - 1 &&
					j > 0 &&
					f[i + 1][j - 1] !== 'm'
				)
					f[i + 1][j - 1] += 1;
				if (i < x - 1 && f[i + 1][j] !== 'm')
					f[i + 1][j] += 1;
				if (
					i < x - 1 &&
					j < y - 1 &&
					f[i + 1][j + 1] !== 'm'
				)
					f[i + 1][j + 1] += 1;
			}
		}
	}
}

function openCells(coords, maxHeight, maxWidth, condition) {
	const cells = document.querySelectorAll('[data-coords]');
	let a = coords[0] - 1;
	let b = coords[1] - 1;
	let flags = 0;

	for (let i = 0; i < 9; i++) {
		if (i && i % 3 === 0) {
			a++;
			b = coords[1] - 1;
		}
		if (
			i !== 4 &&
			a >= 0 &&
			b >= 0 &&
			a < maxHeight &&
			b < maxWidth
		) {
			for (let cell of cells) {
				if (condition === 'countFlags') {
					//CONDITION is countFlags
					if (
						cell.dataset.coords == [a, b].join('-') &&
						cell.classList.contains('flagged')
					) {
						flags++;
					}
				} else if (condition === 'openNearFlags') {
					//CONDITION is openNearFlags
					if (
						cell.dataset.coords == [a, b].join('-') &&
						!cell.classList.contains('flagged')
					) {
						if (cell.dataset.id === 'mine') {
							lose();
							break;
						} else {
							cell.className = 'cell';
						}
					}
				} else if (condition === 'openEmpty') {
					//CONDITION is openEmpty
					if (
						cell.dataset.coords == [a, b].join('-')
					) {
						cell.className = 'cell';
						if (cell.dataset.id === '0') {
							cell.dataset.id += '*';
							openCells(
								cell.dataset.coords.split('-'),
								maxHeight,
								maxWidth,
								'openEmpty'
							);
						}
					}
				}
			}
		}
		b++;
	}
	if (condition === 'countFlags') return flags;
}

function openAllCells(victory) {
	const cells = document.querySelectorAll('[data-coords]');

	cells.forEach((cell) => {
		if (cell.dataset.id === 'mine') {
			cell.classList.replace('cell-covered', 'mine');
			if (victory) cell.classList.add('found');
		} else {
			cell.classList.replace('cell-covered', 'cell');
		}
	});
}

function showCounters() {
	const minesAmount = document.querySelectorAll(
		'[data-id="mine"]'
	);
	const flagsAmount =
		document.querySelectorAll('.flagged');
	const cellsAmount =
		document.querySelectorAll('.cell-covered');

	let minesCount = document.querySelector(
		'[data-value="mines"]'
	);
	let flagsCount = document.querySelector(
		'[data-value="flags"]'
	);

	if (!minesCount && !flagsCount) {
		const wrapper =
			document.querySelector('.information');
		const mines = document.createElement('div');
		const flags = document.createElement('div');
		mines.dataset.value = 'mines';
		flags.dataset.value = 'flags';

		wrapper.insertAdjacentElement('afterbegin', mines);
		wrapper.insertAdjacentElement('beforeend', flags);

		minesCount = mines;
		flagsCount = flags;
	}

	minesCount.textContent = `💣: ${minesAmount.length}`;
	flagsCount.textContent = `🚩: ${flagsAmount.length}`;

	if (cellsAmount.length === minesAmount.length) {
		victory();
	}
}

function removeFieldEvents(selector) {
	const field = document.querySelector(selector);

	field.removeEventListener('click', tableAction);
	field.removeEventListener('contextmenu', setFlag);
	field.removeEventListener('dblclick', openCellsNearFlag);
}

function deleteField(f) {
	removeFieldEvents('[data-dims]');
	f.remove();
}

function countScore(victory) {
	const cells = document.querySelectorAll('[data-coords]');
	let score = 0;
	cells.forEach((cell) => {
		if (!cell.classList.contains('cell-covered'))
			score += 5;
	});
	if (victory)
		alert(
			`Wow! Congratulations! Your score is: ${score} 😃`
		);
	else
		alert(
			`You were so close... Your score is: ${score} 🥺`
		);
}

function lose() {
	removeFieldEvents('[data-dims]');
	countScore(false);
	openAllCells(false);
	document.querySelector('[data-dims]').style.borderColor =
		'red';
}

function victory() {
	removeFieldEvents('[data-dims]');
	countScore(true);
	openAllCells(true);
	document.querySelector('[data-dims]').style.borderColor =
		'green';
}
